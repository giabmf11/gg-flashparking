﻿using System.Threading.Tasks;
using GregGoldsmith.FlashParking.Web.Controllers;
using Shouldly;
using Xunit;

namespace GregGoldsmith.FlashParking.Web.Tests.Controllers
{
    public class HomeController_Tests: FlashParkingWebTestBase
    {
        [Fact]
        public async Task Index_Test()
        {
            //Act
            var response = await GetResponseAsStringAsync(
                GetUrl<HomeController>()
            );

            //Assert
            response.ShouldNotBeNullOrEmpty();
        }
    }
}
