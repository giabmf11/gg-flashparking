using Abp.AspNetCore.TestBase;
using Abp.Modules;
using Abp.Reflection.Extensions;
using GregGoldsmith.FlashParking.Web.Startup;
namespace GregGoldsmith.FlashParking.Web.Tests
{
    [DependsOn(
        typeof(FlashParkingWebModule),
        typeof(AbpAspNetCoreTestBaseModule)
        )]
    public class FlashParkingWebTestModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.UnitOfWork.IsTransactional = false; //EF Core InMemory DB does not support transactions.
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(FlashParkingWebTestModule).GetAssembly());
        }
    }
}