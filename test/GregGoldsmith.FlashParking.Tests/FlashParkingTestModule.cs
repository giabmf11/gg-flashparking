using System.Reflection;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Abp.TestBase;
using GregGoldsmith.FlashParking.EntityFrameworkCore;
using Castle.MicroKernel.Registration;
using Castle.Windsor.MsDependencyInjection;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using GregGoldsmith.FlashParking.Application;
using Abp.Dependency;

namespace GregGoldsmith.FlashParking.Tests
{
    [DependsOn(
        typeof(FlashParkingApplicationModule),
        typeof(FlashParkingEntityFrameworkCoreModule),
        typeof(AbpTestBaseModule)
        )]

    public class FlashParkingTestModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.UnitOfWork.IsTransactional = false; //EF Core InMemory DB does not support transactions.
            SetupInMemoryDb();

            IocManager.Register<IParkingLotAppService, ParkingLotAppService>(DependencyLifeStyle.Transient);
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(FlashParkingTestModule).GetAssembly());
        }

        private void SetupInMemoryDb()
        {
            var services = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase();

            var serviceProvider = WindsorRegistrationHelper.CreateServiceProvider(
                IocManager.IocContainer,
                services
            );

            var builder = new DbContextOptionsBuilder<FlashParkingDbContext>();
            builder.UseInMemoryDatabase("Test").UseInternalServiceProvider(serviceProvider);

            IocManager.IocContainer.Register(
                Component
                    .For<DbContextOptions<FlashParkingDbContext>>()
                    .Instance(builder.Options)
                    .LifestyleSingleton()
            );

            
        }
    }
}