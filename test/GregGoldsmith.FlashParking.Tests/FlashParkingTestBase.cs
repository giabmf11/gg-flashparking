﻿using System;
using System.Threading.Tasks;
using Abp.TestBase;
using GregGoldsmith.FlashParking.EntityFrameworkCore;
using GregGoldsmith.FlashParking.Tests.TestDatas;

namespace GregGoldsmith.FlashParking.Tests
{
    public class FlashParkingTestBase : AbpIntegratedTestBase<FlashParkingTestModule>
    {
        public FlashParkingTestBase()
        {
            UsingDbContext(context => new TestDataBuilder(context).Build());
        }

        protected virtual void UsingDbContext(Action<FlashParkingDbContext> action)
        {
            using (var context = LocalIocManager.Resolve<FlashParkingDbContext>())
            {
                action(context);
                context.SaveChanges();
            }
        }

        protected virtual T UsingDbContext<T>(Func<FlashParkingDbContext, T> func)
        {
            T result;

            using (var context = LocalIocManager.Resolve<FlashParkingDbContext>())
            {
                result = func(context);
                context.SaveChanges();
            }

            return result;
        }

        protected virtual async Task UsingDbContextAsync(Func<FlashParkingDbContext, Task> action)
        {
            using (var context = LocalIocManager.Resolve<FlashParkingDbContext>())
            {
                await action(context);
                await context.SaveChangesAsync(true);
            }
        }

        protected virtual async Task<T> UsingDbContextAsync<T>(Func<FlashParkingDbContext, Task<T>> func)
        {
            T result;

            using (var context = LocalIocManager.Resolve<FlashParkingDbContext>())
            {
                result = await func(context);
                context.SaveChanges();
            }

            return result;
        }
    }
}
