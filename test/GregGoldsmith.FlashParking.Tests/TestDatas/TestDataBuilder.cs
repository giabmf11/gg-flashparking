using GregGoldsmith.FlashParking.Domain;
using GregGoldsmith.FlashParking.EntityFrameworkCore;

namespace GregGoldsmith.FlashParking.Tests.TestDatas
{
    public class TestDataBuilder
    {
        private readonly FlashParkingDbContext _context;

        public TestDataBuilder(FlashParkingDbContext context)
        {
            _context = context;
        }

        public void Build()
        {
            //create test data here...
            _context.ParkingLots.AddRange(
            new ParkingLot("West 6th Parking Garage", "1141 W 6th Street, Austin Tx 78701", 25),
            new ParkingLot("East 7th Self Park", "1234 E 7th Street, Austin TX 78702", 16),
            new ParkingLot("William Cannon Covered Parking", "9984 William Cannon Dr, Austin TX 78745", 30), new ParkingLot("Anderson Mill Transit Parking", "888 Anderson Mill Rd, Austin TX 78759", 12)
            );
        }
    }
}