﻿using GregGoldsmith.FlashParking.Application;
using GregGoldsmith.FlashParking.Application.Dto;
using Shouldly;
using System.Threading.Tasks;
using Xunit;

namespace GregGoldsmith.FlashParking.Tests
{
    public class ParkingLotAppService_Tests : FlashParkingTestBase
    {
        private readonly IParkingLotAppService _parkingLotAppService;
        public ParkingLotAppService_Tests()
        {
            _parkingLotAppService = Resolve<IParkingLotAppService>();
        }

        [Fact]
        public async Task ShouldGetAllParkingLots()
        {
            var output = await _parkingLotAppService.GetAll();
            output.Count.ShouldBe(4);
        }

        [Fact]
        public async Task ShouldGetFilteredParkingLots()
        {
            var output1 = await _parkingLotAppService.GetAll();
            output1.Count.ShouldBe(4);
            ParkingLotDto parking1 = output1[0];
            ParkingLotDto parking2 = output1[1];
            ParkingLotDto parking3 = output1[2];
            //Parking 1
            for (int i = 1; i <= parking1.NumberOfParkingSpaces; i++)
            {
                var parkDto1 = new ParkDto(parking1, new VehicleDto(parking1.Id, "TBR-12" + i.ToString()));
                await _parkingLotAppService.Park(parkDto1);
                parking1 = await _parkingLotAppService.Get(parking1.Id);
            }
            //Parking 2
            for (int i = 1; i <= parking2.NumberOfParkingSpaces; i++)
            {
                var parkDto2 = new ParkDto(parking2, new VehicleDto(parking2.Id, "TBR-12" + i.ToString()));
                await _parkingLotAppService.Park(parkDto2);
                parking2 = await _parkingLotAppService.Get(parking2.Id);
            }
            ParkingLotDto iteratorLot2 = parking2;
            for (int i = 0; i < iteratorLot2.Vehicles.Count; i++)
            {
                await _parkingLotAppService.Leave(parking2, iteratorLot2.Vehicles[i].Id);
                parking2 = await _parkingLotAppService.Get(parking2.Id);
            }
            //Parking 3
            for (int i = 1; i <= parking3.NumberOfParkingSpaces; i++)
            {
                var parkDto3 = new ParkDto(parking3, new VehicleDto(parking3.Id, "TBR-12" + i.ToString()));
                await _parkingLotAppService.Park(parkDto3);
                parking3 = await _parkingLotAppService.Get(parking3.Id);
            }
            ParkingLotDto iteratorLot3 = parking3;
            for (int i = 0; i < 4; i++)
            {
                await _parkingLotAppService.Leave(parking3, iteratorLot3.Vehicles[i].Id);
                parking3 = await _parkingLotAppService.Get(parking3.Id);
            }

            parking1.AvailableParkingSpaces.ShouldBe(0);
            parking2.AvailableParkingSpaces.ShouldBe(parking2.NumberOfParkingSpaces);
            parking3.AvailableParkingSpaces.ShouldBe(parking3.NumberOfParkingSpaces - parking3.Vehicles.Count);
        }
    }
}
