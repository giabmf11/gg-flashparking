using GregGoldsmith.FlashParking.Application;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using GregGoldsmith.FlashParking.Web.Models.Home;
using System;
using System.Collections.Generic;
using GregGoldsmith.FlashParking.Application.Dto;

namespace GregGoldsmith.FlashParking.Web.Controllers
{
    [Route("[controller]")]
    [Route("")]
    public class HomeController : FlashParkingControllerBase
    {
        private readonly IParkingLotAppService _parkingLotAppService;

        public HomeController(IParkingLotAppService parkingLotAppService)
        {
            _parkingLotAppService = parkingLotAppService;
        }

        [HttpGet]  // GET /Home
        public async Task<ActionResult> Index()
        {
            try
            {
                Alerts.Success(L("WelcomeAlertMessage"), L("WelcomeAlertTitle"));
                ViewBag.Title = L("HomeTitle");
                var output = await _parkingLotAppService.GetAll();
                if (output == null || output.Count == 0)
                {
                    Alerts.Warning("NoParkingLotsMessage", "NoParkingLotsTitle");
                    return View(new ParkingLotListModel(new List<ParkingLotDto>()));
                }
                else
                {
                    var model = new ParkingLotListModel(output);
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                this.Logger.Error(ex.ToString());
                Alerts.Danger("ErrorAlertMessage", "ErrorAlertTitle");
                return new ObjectResult(ex);
            }
        }
    }
}