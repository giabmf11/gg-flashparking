using Abp.AspNetCore.Mvc.Controllers;

namespace GregGoldsmith.FlashParking.Web.Controllers
{
    public abstract class FlashParkingControllerBase: AbpController
    {
        protected FlashParkingControllerBase()
        {
            LocalizationSourceName = FlashParkingConsts.LocalizationSourceName;
        }
    }
}