﻿using GregGoldsmith.FlashParking.Application;
using GregGoldsmith.FlashParking.Application.Dto;
using GregGoldsmith.FlashParking.Web.Models.ParkingLot;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Threading.Tasks;

namespace GregGoldsmith.FlashParking.Web.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ParkingLotController : FlashParkingControllerBase
    {
        private IParkingLotAppService _parkingLotAppService;

        public ParkingLotController(IParkingLotAppService parkingLotAppService)
        {
            _parkingLotAppService = parkingLotAppService;
        }

        [HttpGet]
        [Route("Index")]
        public async Task<ActionResult> Index(int parkingLotId)
        {
            try
            {
                ViewBag.Title = L("ParkingLotTitle");
                ParkingLotDto output = await _parkingLotAppService.Get(parkingLotId);
                if (output == null)
                {
                    Alerts.Warning("NoParkingLotsMessage", "NoParkingLotsTitle");
                    return View(new ParkingLotModel(new ParkingLotDto()));
                }
                else
                {
                    var model = new ParkingLotModel(output);
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                this.Logger.Error(ex.ToString());
                Alerts.Danger("ErrorAlertMessage", "ErrorAlertTitle");
                return View(new ParkingLotModel(new ParkingLotDto()));
            }
        }

        [HttpPost]
        [Route("GetParkingLot")]
        public async Task<IActionResult> GetParkingLot([FromBody] int id)
        {
            try
            {
                ParkingLotModel result = new ParkingLotModel(new ParkingLotDto());
                ParkingLotDto output = await _parkingLotAppService.Get(id);

                if (output == null)
                {
                    Alerts.Warning("NoParkingLotsMessage", "NoParkingLotsTitle");
                    return NotFound();
                }
                else
                {
                    var model = new ParkingLotModel(output);
                    return Json(model, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
                }
            }
            catch (Exception ex)
            {
                this.Logger.Error(ex.ToString());
                Alerts.Danger("ErrorAlertMessage", "ErrorAlertTitle");
                return new ObjectResult(ex);
            }
        }

        [HttpPost]
        [Route("Park")]
        public async Task<IActionResult> Park([FromBody] ParkModel parkModel)
        {
            try
            {
                await _parkingLotAppService.Park(new ParkDto(parkModel.ParkingLot, parkModel.Vehicle));

                return Json(true, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
            }
            catch (Exception ex)
            {
                this.Logger.Error(ex.ToString());
                Alerts.Danger("ErrorAlertMessage", "ErrorAlertTitle");
                return new ObjectResult(ex);
            }
        }

        [HttpPost]
        [Route("Leave")]
        public async Task<IActionResult> Leave([FromBody] LeaveModel leaveModel)
        {
            try
            {
                await _parkingLotAppService.Leave(leaveModel.ParkingLot, leaveModel.VehicleId);

                return Json(true, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
            }
            catch (Exception ex)
            {
                this.Logger.Error(ex.ToString());
                Alerts.Danger("ErrorAlertMessage", "ErrorAlertTitle");
                return new ObjectResult(ex);
            }
        }
    }
}
