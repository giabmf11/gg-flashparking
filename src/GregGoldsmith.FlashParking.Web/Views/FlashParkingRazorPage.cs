﻿using Abp.AspNetCore.Mvc.Views;

namespace GregGoldsmith.FlashParking.Web.Views
{
    public abstract class FlashParkingRazorPage<TModel> : AbpRazorPage<TModel>
    {
        protected FlashParkingRazorPage()
        {
            LocalizationSourceName = FlashParkingConsts.LocalizationSourceName;
        }
    }
}
