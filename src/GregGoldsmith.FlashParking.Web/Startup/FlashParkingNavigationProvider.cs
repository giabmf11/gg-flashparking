﻿using Abp.Application.Navigation;
using Abp.Localization;

namespace GregGoldsmith.FlashParking.Web.Startup
{
    /// <summary>
    /// This class defines menus for the application.
    /// </summary>
    public class FlashParkingNavigationProvider : NavigationProvider
    {
        public override void SetNavigation(INavigationProviderContext context)
        {
            context.Manager.MainMenu
                .AddItem(
                    new MenuItemDefinition(
                        PageNames.Home,
                        L("HomePage"),
                        url: "",
                        icon: "fa fa-home"
                        )
                );
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, FlashParkingConsts.LocalizationSourceName);
        }
    }
}
