﻿using Abp.AspNetCore;
using Abp.AspNetCore.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using GregGoldsmith.FlashParking.Configuration;
using GregGoldsmith.FlashParking.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.ApplicationParts;
using Microsoft.Extensions.Configuration;

namespace GregGoldsmith.FlashParking.Web.Startup
{
    [DependsOn(
        typeof(FlashParkingApplicationModule), 
        typeof(FlashParkingEntityFrameworkCoreModule), 
        typeof(AbpAspNetCoreModule))]
    public class FlashParkingWebModule : AbpModule
    {
        private readonly IConfigurationRoot _appConfiguration;

        public FlashParkingWebModule(IWebHostEnvironment env)
        {
            _appConfiguration = AppConfigurations.Get(env.ContentRootPath, env.EnvironmentName);
        }

        public override void PreInitialize()
        {
            Configuration.DefaultNameOrConnectionString = _appConfiguration.GetConnectionString(FlashParkingConsts.ConnectionStringName);

            Configuration.Navigation.Providers.Add<FlashParkingNavigationProvider>();

            Configuration.Modules.AbpAspNetCore()
                .CreateControllersForAppServices(
                    typeof(FlashParkingApplicationModule).GetAssembly()
                );
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(FlashParkingWebModule).GetAssembly());
        }

        public override void PostInitialize()
        {
            IocManager.Resolve<ApplicationPartManager>()
                .AddApplicationPartsIfNotAddedBefore(typeof(FlashParkingWebModule).Assembly);
        }
    }
}
