﻿var ParkingLot = function (p, e) {
    var parkingLot = p.ParkingLot;
    var invalidLicensePlateMessage = e;
    var vehicles = [];
    var currentVehicle = null;
    var addVehicleButton = document.querySelector('#vehicleButton');
    var minLicenseLength = 6;
    var maxLicenseLength = 10;


    initialize();

    function initialize() {
        addVehicleButton.addEventListener('click', addVehicle);
        if (parkingLot.Vehicles !== undefined) {
            vehicles = parkingLot.Vehicles;
            renderVehiclesTable();
        }
    }

    function addVehicle() {
        let newLicense = document.querySelector("#vehicleValue").value;

        if (newLicense.length < minLicenseLength || newLicense.length > maxLicenseLength) {
            abp.notify.error(invalidLicensePlateMessage);
        }
        else {
            let newVehicle = {
                License: newLicense,
                CreationTime: new Date(),
                DepartureTime: '',
                ParkingLotId: parkingLot.Id,
                Id: 0
            };
            let parkModel = {
                ParkingLot: parkingLot,
                Vehicle: newVehicle
            };
            let fromBodyData = JSON.stringify(parkModel);

            abp.ajax({
                url: '/ParkingLot/Park',
                data: fromBodyData
            }).done(function (data) {
                if (data.Message !== undefined && data.Message.length > 0) {
                    abp.notify.error(data.Message);
                }
                else {
                    document.querySelector("#vehicleValue").value = '';
                    abp.notify.success('Your car is parked successfully');                    
                    getData();
                }
            });
        }
    }

    this.leaveParkingLot = function (event) {
        currentVehicle = vehicles[event.target.dataset.row];

        let leaveModel = {
            ParkingLot: parkingLot,
            VehicleId: currentVehicle.Id
        }

        let fromBodyData = JSON.stringify(leaveModel);

        abp.ajax({
            url: '/ParkingLot/Leave',
            data: fromBodyData
        }).done(function (data) {
            if (data.Message !== undefined && data.Message.length > 0) {
                abp.notify.error(data.Message);
            }
            else {
                abp.notify.success('Thanks for parking with us. Come back soon!');                
                getData();
                printSummary(currentVehicle);
            }
        });
    }

    function getData() {
        abp.ajax({
            url: '/ParkingLot/GetParkingLot',
            data: JSON.stringify(parkingLot.Id)
        }).done(function (data) {
            parkingLot = data.Result.ParkingLot;
            vehicles = parkingLot.Vehicles;
            displayParkingChange();
        });
    }

    function displayParkingChange() {
        setClassForBadge();
        toggleLotFull();
        renderVehiclesTable();
    }

    function setClassForBadge() {
        let newStatus = parkingLot.AvailableParkingSpaces == 0 ? 'bg-danger' : 'bg-success';
        let oldStatus = parkingLot.AvailableParkingSpaces == 0 ? 'bg-success' : 'bg-danger';
        document.querySelector('#placesCount').classList.remove(oldStatus);
        document.querySelector('#placesCount').classList.add(newStatus);
        document.querySelector('#availableParkingSpaces').innerHTML = parkingLot.AvailableParkingSpaces;
    }

    function toggleLotFull() {
        if (parkingLot.AvailableParkingSpaces === 0) {
            addVehicleButton.setAttribute('disabled', true);
        } else {
            addVehicleButton.removeAttribute('disabled');
        }
    }

    function renderVehiclesTable() {
        let results = '';
        if (vehicles === undefined || vehicles.length === 0) {
            document.querySelector("#parking tbody").innerHTML = '';
            document.querySelector("#parking").style.display = 'none';
        }
        else {
            for (var i = 0; i < vehicles.length; i++) {
                results +=
                    `<tr>
                      <td>${vehicles[i].Id}</td>
                      <td>${vehicles[i].License}</td>
                      <td>${formatDate(vehicles[i].CreationTime)}</td>
                      <td>Still Parked</td>
                      <td class="text-right">
                        <button data-row="${i}" onClick="pageLeaveParkingLot(event, ${i})" data-toggle="modal" data-target="#myModal" class="btn btn-primary">Leave</button>
                      </td>
                    </tr>`;
            }
            document.querySelector("#parking tbody").innerHTML = results;
            document.querySelector("#parking").style.display = 'inline-table';
        }
    }

    function printSummary(vehicle) {
        document.querySelector("#receiptLicensePlate").textContent = vehicle.License;
        document.querySelector("#receiptArrival").textContent = formatDate(vehicle.CreationTime);
        document.querySelector("#receiptDeparture").textContent = formatDate(vehicle.DepartureTime);
        document.querySelector("#receiptBilledHours").textContent = calculateHoursBilled(vehicle);
    }

    function calculateHoursBilled(vehicle) {
        let arrivedAt = new Date(vehicle.CreationTime).getTime();
        let leftAt = new Date(vehicle.DepartureTime).getTime();
        return secondsToHours((leftAt - arrivedAt) / 1000); //duration in seconds
    }

    function setLeaveTime(event) {
        vehicles[event.target.dataset.row].DepartureDate = new Date(Date.now());
    }

    function formatDate(objDate) {
        var date = new Date(objDate);
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'PM' : 'AM';
        hours = hours % 12;
        hours = hours ? hours : 12;
        minutes = minutes < 10 ? '0' + minutes : minutes;
        var strTime = hours + ':' + minutes + ' ' + ampm;
        return (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear() + " " + strTime;
    }

    function secondsToHours(d) {
        d = Number(d);
        let h = Math.ceil(d / 3600);
        return h;
    }


}