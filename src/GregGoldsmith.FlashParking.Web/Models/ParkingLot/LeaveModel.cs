﻿using GregGoldsmith.FlashParking.Application.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GregGoldsmith.FlashParking.Web.Models.ParkingLot
{
    public class LeaveModel
    {
        public ParkingLotDto ParkingLot { get; set; }
        public int VehicleId { get; set; }
        public LeaveModel(ParkingLotDto parkingLot, int vehicleId)
        {
            this.ParkingLot = parkingLot;
            this.VehicleId = vehicleId;
        }
    }
}
