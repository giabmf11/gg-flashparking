﻿using GregGoldsmith.FlashParking.Application.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GregGoldsmith.FlashParking.Web.Models.ParkingLot
{
    public class ParkModel
    {
        public ParkingLotDto ParkingLot { get; set; }
        public VehicleDto Vehicle { get; set; }
        public ParkModel(ParkingLotDto parkingLot, VehicleDto vehicle)
        {
            this.ParkingLot = parkingLot;
            this.Vehicle = vehicle;
        }
    }
}
