﻿using GregGoldsmith.FlashParking.Application.Dto;
using GregGoldsmith.FlashParking.Domain;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GregGoldsmith.FlashParking.Web.Models.ParkingLot
{
    public class ParkingLotModel
    {
        public ParkingLotDto ParkingLot { get; }

        public ParkingLotModel(ParkingLotDto parkingLot)
        {
            ParkingLot = parkingLot;
        }

        public string GetBadgeClass(int availableSpaces)
        {
            if (availableSpaces == 0)
                return "bg-danger";
            else
                return "bg-success";
        }

        public string ConvertThisToJson()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
