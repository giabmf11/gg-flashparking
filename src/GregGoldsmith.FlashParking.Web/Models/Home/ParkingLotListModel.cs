﻿using GregGoldsmith.FlashParking.Application.Dto;
using GregGoldsmith.FlashParking.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GregGoldsmith.FlashParking.Web.Models.Home
{
    public class ParkingLotListModel
    {
        public IReadOnlyList<ParkingLotDto> ParkingLots { get; }

        public ParkingLotListModel(IReadOnlyList<ParkingLotDto> parkingLots)
        {
            ParkingLots = parkingLots;
        }

        public string GetBadgeClass(int availableSpaces)
        {
            if (availableSpaces == 0)
                return "bg-danger";
            else
                return "bg-success";
        }
    }
}
