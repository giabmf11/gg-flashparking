﻿using Microsoft.EntityFrameworkCore;

namespace GregGoldsmith.FlashParking.EntityFrameworkCore
{
    public static class DbContextOptionsConfigurer
    {
        public static void Configure(
            DbContextOptionsBuilder<FlashParkingDbContext> dbContextOptions, 
            string connectionString
            )
        {
            /* This is the single point to configure DbContextOptions for FlashParkingDbContext */
            dbContextOptions.UseSqlServer(connectionString);
        }
    }
}
