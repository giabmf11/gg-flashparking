﻿using GregGoldsmith.FlashParking.Configuration;
using GregGoldsmith.FlashParking.Web;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace GregGoldsmith.FlashParking.EntityFrameworkCore
{
    /* This class is needed to run EF Core PMC commands. Not used anywhere else */
    public class FlashParkingDbContextFactory : IDesignTimeDbContextFactory<FlashParkingDbContext>
    {
        public FlashParkingDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<FlashParkingDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            DbContextOptionsConfigurer.Configure(
                builder,
                configuration.GetConnectionString(FlashParkingConsts.ConnectionStringName)
            );

            return new FlashParkingDbContext(builder.Options);
        }
    }
}