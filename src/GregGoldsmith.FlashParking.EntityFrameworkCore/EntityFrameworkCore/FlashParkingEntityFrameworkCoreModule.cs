﻿using Abp.EntityFrameworkCore;
using Abp.Modules;
using Abp.Reflection.Extensions;

namespace GregGoldsmith.FlashParking.EntityFrameworkCore
{
    [DependsOn(
        typeof(FlashParkingCoreModule), 
        typeof(AbpEntityFrameworkCoreModule))]
    public class FlashParkingEntityFrameworkCoreModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(FlashParkingEntityFrameworkCoreModule).GetAssembly());
        }
    }
}