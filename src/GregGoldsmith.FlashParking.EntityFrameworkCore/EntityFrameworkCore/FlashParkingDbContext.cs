﻿using Abp.EntityFrameworkCore;
using GregGoldsmith.FlashParking.Domain;
using Microsoft.EntityFrameworkCore;

namespace GregGoldsmith.FlashParking.EntityFrameworkCore
{
    public class FlashParkingDbContext : AbpDbContext
    {
        public DbSet<ParkingLot> ParkingLots { get; set; }
        public DbSet<Vehicle> Vehicles { get; set; }

        public FlashParkingDbContext(DbContextOptions<FlashParkingDbContext> options) 
            : base(options)
        {

        }
    }
}
