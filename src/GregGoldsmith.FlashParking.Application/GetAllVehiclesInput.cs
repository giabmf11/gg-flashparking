﻿namespace GregGoldsmith.FlashParking.Application
{
    public class GetAllVehiclesInput
    {
        public int? ParkingLotId { get; set; }
    }
}