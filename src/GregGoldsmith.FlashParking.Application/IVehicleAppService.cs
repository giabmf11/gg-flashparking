﻿using Abp.Application.Services.Dto;
using GregGoldsmith.FlashParking.Application.Dto;
using System.Threading.Tasks;

namespace GregGoldsmith.FlashParking.Application
{
    public interface IVehicleAppService
    {
        Task<VehicleDto> Get(int id);

        Task<ListResultDto<VehicleDto>> GetAll(GetAllVehiclesInput input);

        Task<int> InsertAndGetId(VehicleDto input);

        Task Delete(int id);
    }
}