﻿using Abp.Application.Services;

namespace GregGoldsmith.FlashParking
{
    /// <summary>
    /// Derive your application services from this class.
    /// </summary>
    public abstract class FlashParkingAppServiceBase : ApplicationService
    {
        protected FlashParkingAppServiceBase()
        {
            LocalizationSourceName = FlashParkingConsts.LocalizationSourceName;
        }
    }
}