﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using GregGoldsmith.FlashParking.Domain;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GregGoldsmith.FlashParking.Application.Dto
{
    [AutoMapFrom(typeof(ParkingLot))]
    [AutoMapTo(typeof(ParkingLot))]
    public class ParkingLotDto : EntityDto
    {
        public const int MaxGeneralLength = ParkingLot.MaxGeneralLength;

        public string Name { get; set; }

        public string Address { get; set; }

        public int NumberOfParkingSpaces { get; set; }

        public int AvailableParkingSpaces { get; set; }

        public List<VehicleDto> Vehicles { get; set; }
    }
}