﻿using Abp.Application.Services;
using GregGoldsmith.FlashParking.Application.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GregGoldsmith.FlashParking.Application
{
    public interface IParkingLotAppService : IApplicationService
    {
        Task<List<ParkingLotDto>> GetAll();
        Task<ParkingLotDto> Get(int id);
        Task<bool> Park(ParkDto parkDto);
        Task<bool> Leave(ParkingLotDto parkingLot, int vehicleId);
    }
}
