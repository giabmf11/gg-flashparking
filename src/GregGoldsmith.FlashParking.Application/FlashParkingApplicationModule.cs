﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;

namespace GregGoldsmith.FlashParking
{
    [DependsOn(
        typeof(FlashParkingCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class FlashParkingApplicationModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(FlashParkingApplicationModule).GetAssembly());
        }
    }
}