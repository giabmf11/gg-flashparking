﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.ObjectMapping;
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using GregGoldsmith.FlashParking.Domain;
using GregGoldsmith.FlashParking.Application.Dto;
using System.Collections.Generic;
using Abp.Domain.Uow;

namespace GregGoldsmith.FlashParking.Application
{
    public class ParkingLotAppService : IParkingLotAppService
    {
        private readonly IRepository<ParkingLot> _parkingLotRepository;
        private readonly IRepository<Vehicle> _vehicleRepository;
        private readonly IObjectMapper _objectMapper;

        public ParkingLotAppService(IRepository<ParkingLot> parkingLotRepository,
            IRepository<Vehicle> vehicleRepository,
            IObjectMapper objectMapper)
        {
            _parkingLotRepository = parkingLotRepository;
            _vehicleRepository = vehicleRepository;
            _objectMapper = objectMapper;
        }

        public async Task<ParkingLotDto> Get(int id)
        {
            ParkingLotDto result = null;
            var parkingLot = await _parkingLotRepository.GetAll()
                .Where(p => p.Id == id)
                .Include(p => p.Vehicles)
                .FirstOrDefaultAsync();

            if (parkingLot != default)
            {
                result = _objectMapper.Map<ParkingLotDto>(parkingLot);
            }
            return result;
        }

        [UnitOfWork]
        public async Task<List<ParkingLotDto>> GetAll()
        {
            List<ParkingLotDto> result = new List<ParkingLotDto>();
            var parkingLots = await _parkingLotRepository
                .GetAll()
                .Include(p => p.Vehicles)
                .OrderByDescending(t => t.Id)
                .ToListAsync();

            if (parkingLots != null && parkingLots.Count != 0)
            {
                result = _objectMapper.Map<List<ParkingLotDto>>(parkingLots);
            }
            return result;
        }

        [UnitOfWork]
        public async Task<bool> Leave(ParkingLotDto parkingLot, int id)
        {
            var parkingLotEntity = _objectMapper.Map<ParkingLot>(parkingLot);
            var vehicleEntity = parkingLotEntity.Vehicles.Find(v => v.Id == id);

            if (vehicleEntity == null)
                return false;

            parkingLotEntity.AvailableParkingSpaces = parkingLotEntity.NumberOfParkingSpaces - parkingLotEntity.Vehicles.Count;
            if (parkingLotEntity.AvailableParkingSpaces == parkingLotEntity.NumberOfParkingSpaces)
                throw new ApplicationException("Lot is empty. No more parking spaces are available.");

            parkingLotEntity.AvailableParkingSpaces++;
            parkingLotEntity.Vehicles.Remove(vehicleEntity);
            await _vehicleRepository.DeleteAsync(id);
            await _parkingLotRepository.UpdateAsync(parkingLotEntity);

            return true;
        }

        [UnitOfWork]
        public async Task<bool> Park(ParkDto parkDto)
        {
            var parkingLotEntity = _objectMapper.Map<ParkingLot>(parkDto.ParkingLot);
            var vehicleEntity = _objectMapper.Map<Vehicle>(parkDto.Vehicle);

            parkingLotEntity.AvailableParkingSpaces = parkingLotEntity.NumberOfParkingSpaces - parkingLotEntity.Vehicles.Count;
            if (parkingLotEntity.AvailableParkingSpaces == 0)
                throw new ApplicationException("Lot is full. No more parking spaces are available.");

            parkingLotEntity.AvailableParkingSpaces--;
            await _vehicleRepository.InsertAsync(vehicleEntity);
            await _parkingLotRepository.UpdateAsync(parkingLotEntity);
            return true;
        }
    }
}
