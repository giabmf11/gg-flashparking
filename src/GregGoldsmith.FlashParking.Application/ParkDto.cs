﻿using GregGoldsmith.FlashParking.Application.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GregGoldsmith.FlashParking
{
    public class ParkDto
    {
        public ParkingLotDto ParkingLot { get; set; }
        public VehicleDto Vehicle { get; set; }

        public ParkDto(ParkingLotDto parkingLot, VehicleDto vehicle)
        {
            ParkingLot = parkingLot;
            Vehicle = vehicle;
        }
    }
}
