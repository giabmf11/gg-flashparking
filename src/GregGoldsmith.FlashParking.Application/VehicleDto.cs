﻿using Abp.AutoMapper;
using GregGoldsmith.FlashParking.Domain;
using System;
using System.ComponentModel.DataAnnotations;

namespace GregGoldsmith.FlashParking.Application.Dto
{
    [AutoMapFrom(typeof(Vehicle))]
    [AutoMapTo(typeof(Vehicle))]
    public class VehicleDto
    {
        public const int MinLicenseLength = Vehicle.MinLicenseLength;
        public const int MaxLicenseLength = Vehicle.MaxLicenseLength;

        public string License { get; set; }

        public int Id { get; set; }

        public int ParkingLotId { get; set; }

        public DateTime CreationTime { get; set; }

        public VehicleDto() { }

        public VehicleDto(int parkingLotId, string license)
        {
            ParkingLotId = parkingLotId;
            License = license;
        }
    }
}