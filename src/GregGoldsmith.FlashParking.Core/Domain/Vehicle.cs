﻿using Abp.Domain.Entities;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Timing;

namespace GregGoldsmith.FlashParking.Domain
{
    [Table("Vehicles")]
    public class Vehicle : Entity, IHasCreationTime
    {
        public const int MinLicenseLength = 6;
        public const int MaxLicenseLength = 10;

        [Required]
        [StringLength(MaxLicenseLength, MinimumLength = MinLicenseLength)]
        public string License { get; set; } 

        [Required]
        [ForeignKey("ParkingLot")]
        public int ParkingLotId { get; set; }

        public DateTime CreationTime { get; set; }

        public Vehicle()
        {
            CreationTime = Clock.Now;
        }

        public Vehicle(int parkingLotId, string license) 
            : this()
        {
            ParkingLotId = parkingLotId;
            License = license;
        }
    }
}
