﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;

namespace GregGoldsmith.FlashParking.Domain
{
    [Table("ParkingLots")]
    public class ParkingLot : Entity
    {
        public const int MaxGeneralLength = 256;

        [Required]
        [StringLength(MaxGeneralLength)]
        public string Name { get; set; }

        [Required]
        [StringLength(MaxGeneralLength)]
        public string Address { get; set; }

        [Required]
        public int NumberOfParkingSpaces { get; set; }

        public int AvailableParkingSpaces { get; set; }

        public List<Vehicle> Vehicles { get; set; }

        public ParkingLot()
        {
        }

        public ParkingLot(string name, string address, int numberOfParkingSpaces)
            : this()
        {
            Name = name;
            Address = address;
            NumberOfParkingSpaces = numberOfParkingSpaces;
            AvailableParkingSpaces = numberOfParkingSpaces;            
        }
    }
}

