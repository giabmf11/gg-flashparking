﻿using Abp.Modules;
using Abp.Reflection.Extensions;
using GregGoldsmith.FlashParking.Localization;

namespace GregGoldsmith.FlashParking
{
    public class FlashParkingCoreModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Auditing.IsEnabledForAnonymousUsers = true;

            FlashParkingLocalizationConfigurer.Configure(Configuration.Localization);
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(FlashParkingCoreModule).GetAssembly());
        }
    }
}